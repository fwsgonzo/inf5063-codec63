#include <inttypes.h>
#include <math.h>
#include <stdlib.h>
#include <x86intrin.h>
#include <immintrin.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "dsp.h"
#include "tables.h"

static void transpose_block(float *in_data, float *out_data)
{
	/*
  int i, j;
  for (i = 0; i < 8; ++i)
  {
    for (j = 0; j < 8; ++j)
    {
      out_data[i*8+j] = in_data[j*8+i];
    }
  }*/
	
	// measured with -O2 -ftree-vectorize to be 7.22 times faster
	// than GCC version
	// implementation from:
	// http://stackoverflow.com/questions/16941098/fast-memory-transpose-with-sse-avx-and-openmp
	// ideas from:
	// https://software.intel.com/en-us/articles/benefits-of-intel-avx-for-small-matrices
	
	__m256 row0 = _mm256_load_ps(in_data +  0);
	__m256 row1 = _mm256_load_ps(in_data +  8);
	__m256 row2 = _mm256_load_ps(in_data + 16);
	__m256 row3 = _mm256_load_ps(in_data + 24);
	__m256 row4 = _mm256_load_ps(in_data + 32);
	__m256 row5 = _mm256_load_ps(in_data + 40);
	__m256 row6 = _mm256_load_ps(in_data + 48);
	__m256 row7 = _mm256_load_ps(in_data + 56);
	
	__m256 t0 = _mm256_unpacklo_ps(row0, row1);
	__m256 t1 = _mm256_unpackhi_ps(row0, row1);
	__m256 t2 = _mm256_unpacklo_ps(row2, row3);
	__m256 t3 = _mm256_unpackhi_ps(row2, row3);
	__m256 t4 = _mm256_unpacklo_ps(row4, row5);
	__m256 t5 = _mm256_unpackhi_ps(row4, row5);
	__m256 t6 = _mm256_unpacklo_ps(row6, row7);
	__m256 t7 = _mm256_unpackhi_ps(row6, row7);
	
	__m256 tt0 = _mm256_shuffle_ps(t0,t2,_MM_SHUFFLE(1,0,1,0));
	__m256 tt1 = _mm256_shuffle_ps(t0,t2,_MM_SHUFFLE(3,2,3,2));
	__m256 tt2 = _mm256_shuffle_ps(t1,t3,_MM_SHUFFLE(1,0,1,0));
	__m256 tt3 = _mm256_shuffle_ps(t1,t3,_MM_SHUFFLE(3,2,3,2));
	__m256 tt4 = _mm256_shuffle_ps(t4,t6,_MM_SHUFFLE(1,0,1,0));
	__m256 tt5 = _mm256_shuffle_ps(t4,t6,_MM_SHUFFLE(3,2,3,2));
	__m256 tt6 = _mm256_shuffle_ps(t5,t7,_MM_SHUFFLE(1,0,1,0));
	__m256 tt7 = _mm256_shuffle_ps(t5,t7,_MM_SHUFFLE(3,2,3,2));
	
	_mm256_store_ps(out_data +  0, _mm256_permute2f128_ps(tt0, tt4, 0x20));
	_mm256_store_ps(out_data +  8, _mm256_permute2f128_ps(tt1, tt5, 0x20));
	_mm256_store_ps(out_data + 16, _mm256_permute2f128_ps(tt2, tt6, 0x20));
	_mm256_store_ps(out_data + 24, _mm256_permute2f128_ps(tt3, tt7, 0x20));
	_mm256_store_ps(out_data + 32, _mm256_permute2f128_ps(tt0, tt4, 0x31));
	_mm256_store_ps(out_data + 40, _mm256_permute2f128_ps(tt1, tt5, 0x31));
	_mm256_store_ps(out_data + 48, _mm256_permute2f128_ps(tt2, tt6, 0x31));
	_mm256_store_ps(out_data + 56, _mm256_permute2f128_ps(tt3, tt7, 0x31));
}

static void dct_1d(float *in_data, float *out_data)
{
	/*
  int i, j;

  for (i = 0; i < 8; ++i)
  {
    float dct = 0;

    for (j = 0; j < 8; ++j)
    {
      dct += in_data[j] * dctlookup_transp[i][j];
    }

    out_data[i] = dct;
  }*/
	int row;
	__m256 dct_row[8];
	for (row = 0; row < 8; ++row)
	{
		dct_row[row] = _mm256_load_ps(dctlookup[row]);
	}
	__m256 dct;
	__m256 xmm0;
	
	for (row = 0; row < 8; row++)
	{
		dct = _mm256_setzero_ps();
		
		for (int col = 0; col < 8; ++col)
		{
			xmm0 = _mm256_broadcast_ss(in_data + col);
			
			// in_data[8] *= dct[col][8]
			xmm0 = _mm256_mul_ps(xmm0, dct_row[col]);
			// idct[8] += in_data[8]
			dct = _mm256_add_ps(dct, xmm0);
			
			//dct = _mm256_fmadd_ps(xmm0, dct_row[col], dct);
		}
		
		_mm256_store_ps(out_data, dct);
		out_data += 8;
		in_data += 8;
	}
  
}
static void dct_1d_transp(float* in_data, float* out_data)
{
  /*int i, j;

  for (int row = 0; row < 8; ++row)
  for (i = 0; i < 8; ++i)
  {
    float dct = 0;
	
    for (j = 0; j < 8; ++j)
    {
      dct += in_data[j*8 + row] * dctlookup_transp[i][j];
    }
	
    out_data[i*8 + row] = dct;
  }
  */
	__m256 dct;
	__m256 dct_row;
	__m256 xmm0;
	
	for (int i = 0; i < 8; i++)
	{
		dct = _mm256_setzero_ps();
		
		for (int j = 0; j < 8; j++)
		{
			dct_row = _mm256_broadcast_ss(dctlookup_transp[i] + j);
			
			xmm0 = _mm256_load_ps(in_data + j*8);
			
			xmm0 = _mm256_mul_ps(xmm0, dct_row);
			dct  = _mm256_add_ps(dct, xmm0);
			
			//dct = _mm256_fmadd_ps(xmm0, dct_row, dct);
		}
		
		_mm256_store_ps(out_data + i*8, dct);
	}
	
}

static void idct_1d(float *in_data, float *out_data)
{
	/*
	for (i = 0; i < 8; ++i)
	{
		float idct = 0;
		
		for (j = 0; j < 8; ++j)
		{
			idct += in_data[j] * dctlookup[i][j];
		}
		
		out_data[i] = idct;
	}*/
	
	__m256 dct_row[8];
	for (int col = 0; col < 8; ++col)
	{
		dct_row[col] = _mm256_load_ps(dctlookup_transp[col]);
	}
	__m256 idct;
	__m256 xmm0;
	
	for (int row = 0; row < 8; row++)
	{
		idct = _mm256_setzero_ps();
		
		for (int col = 0; col < 8; ++col)
		{
			xmm0 = _mm256_broadcast_ss(in_data + col);
			
			// in_data *= dct[col]
			xmm0 = _mm256_mul_ps(xmm0, dct_row[col]);
			// idct += (in_data * dct_row)
			idct = _mm256_add_ps(idct, xmm0);
			
			//idct = _mm256_fmadd_ps(xmm0, dct_row[col], idct);
		}
		
		_mm256_store_ps(out_data, idct);
		out_data += 8;
		in_data += 8;
	}
}
static void idct_1d_transp(float* in_data, float* out_data)
{
  /*int i, j;

  for (int row = 0; row < 8; ++row)
  for (i = 0; i < 8; ++i)
  {
    float dct = 0;
	
    for (j = 0; j < 8; ++j)
    {
      dct += in_data[j*8 + row] * dctlookup[i][j];
    }
	
    out_data[i*8 + row] = dct;
  }*/
  
	__m256 idct;
	__m256 dct_row;
	__m256 xmm0;
	
	for (int i = 0; i < 8; i++)
	{
		idct = _mm256_setzero_ps();
		
		for (int j = 0; j < 8; j++)
		{
			dct_row = _mm256_broadcast_ss(dctlookup[i] + j);
			xmm0 = _mm256_load_ps(in_data + j * 8);
			
			// idct += in_data[j*8] * dctlookup[i];
			xmm0 = _mm256_mul_ps(xmm0, dct_row);
			idct = _mm256_add_ps(idct, xmm0);
			
			//idct = _mm256_fmadd_ps(xmm0, dct_row, idct);
		}
		
		_mm256_store_ps(out_data + i*8, idct);
	}
	
}

static void scale_block(float *in_data, float *out_data)
{
	// apparently not very fast :/
	/*__m256 rows[8];
	rows[0] = _mm256_load_ps(in_data);
	__m256 zrow = _mm256_set_ps(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, ISQRT2);
	__m256 zcol = _mm256_set1_ps(ISQRT2);
	
	// first row
	rows[0] = _mm256_mul_ps(rows[0], zrow); // row
	rows[0] = _mm256_mul_ps(rows[0], zcol); // col
	
	int v;
	// remaining 7 rows
	for (v = 1; v < 8; v++)
	{
		rows[v] = _mm256_load_ps(in_data + v*8);
		rows[v] = _mm256_mul_ps(rows[v], zrow); // row
	}
	for (v = 0; v < 8; v++)
		_mm256_store_ps(out_data + v*8, rows[v]);
	*/
  
  int u, v;
  for (v = 0; v < 8; ++v)
  {
    for (u = 0; u < 8; ++u)
    {
      float a1 = (u) ? 1.0f : ISQRT2;
      float a2 = (v) ? 1.0f : ISQRT2;
	  
	  //float v1 = in_data[v*8+u] * a1 * a2;
      //float v2 = out_data[v*8+u];
	  //printf("%f  vs  %f \n", v1, v2);
	  //assert(v1 == v2);
	  
      // Scale according to normalizing function //
	  out_data[v*8+u] = in_data[v*8+u] * a1 * a2;
    }
  }
}

static void quantize_block(float *in_data, float *out_data, uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    uint8_t u = zigzag_U[zigzag];
    uint8_t v = zigzag_V[zigzag];

    float dct = in_data[v*8+u];

    /* Zig-zag and quantize */
    out_data[zigzag] = (float) round((dct / 4.0) / quant_tbl[zigzag]);
  }
}

static void dequantize_block(float *in_data, float *out_data,
    uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    uint8_t u = zigzag_U[zigzag];
    uint8_t v = zigzag_V[zigzag];

    float dct = in_data[zigzag];

    /* Zig-zag and de-quantize */
    out_data[v*8+u] = (float) round((dct * quant_tbl[zigzag]) / 4.0);
  }
}

void dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(32)));
  float mb2[8*8] __attribute((aligned(32)));

  int i;
  for (i = 0; i < 64; ++i) { mb2[i] = in_data[i]; }

  /* Two 1D DCT operations with transpose */
  dct_1d(mb2, mb);
  dct_1d_transp(mb, mb2);

  scale_block(mb2, mb);
  quantize_block(mb, mb2, quant_tbl);

  for (i = 0; i < 64; ++i) { out_data[i] = mb2[i]; }
}

void dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(32)));
  float mb2[8*8] __attribute((aligned(32)));

  int i;
  for (i = 0; i < 64; ++i) { mb[i] = (float) in_data[i]; }

  dequantize_block(mb, mb2, quant_tbl);
  scale_block(mb2, mb);

  /* Two 1D inverse DCT operations with transpose */
  idct_1d(mb, mb2);
  idct_1d_transp(mb2, mb);

  for (i = 0; i < 64; ++i) { out_data[i] = mb[i]; }
}

int sad_block_8x8(uint8_t *block1, uint8_t *block2, int stride)
{
	__m128i sad[4];
	__m128i v1[4];
	__m128i v2[4];
	
	for (int v = 0; v < 4; ++v)
	{
		v1[v] = _mm_set_epi64(*(__m64 *) (block1),
							  *(__m64 *) (block1 + stride));
		block1 += stride * 2;
		v2[v] = _mm_set_epi64(*(__m64 *) (block2),
							  *(__m64 *) (block2 + stride));
		block2 += stride * 2;
		
		// sum of absolute differences
		// NOTE: high latency, but good throughput
		sad[v] = _mm_sad_epu8(v1[v], v2[v]);
	}
	
	// the sum of absolute differences can only be positive
	// so we cheat a little bit and pretend its 4x 32bit ints
	// [!] this coincides with the return value
	typedef union
	{
		__m128i i128;
		int32_t elem[4];
		
	} result_t;
	result_t sum;
	
	// result += 
	sum.i128 = sad[0];
	sum.i128 = _mm_add_epi32(sum.i128, sad[1]);
	sum.i128 = _mm_add_epi32(sum.i128, sad[2]);
	sum.i128 = _mm_add_epi32(sum.i128, sad[3]);
	
	// return result
	return sum.elem[0] + sum.elem[2];
}
